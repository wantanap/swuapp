<!DOCTYPE html>
<html lang="en">
<head>
<br>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
      
    <title>Document</title>
</head>
<body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

<form  class="needs-validation" novalidate >
<div class="container">

    <div class="row">  
        <div class "col-sm-2">ชื่อ:</div>
        <div class="col-sm-4" > 
            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="กรุณาใส่ชื่อ" required>
            <div class="invalid-feedback">
                กรุณาใส่ชื่อ
            </div>
        </div>
        <div class="col-sm-2">นามสกุล:</div>
        <div class="col-sm-4">
            <input type="text"  class="form-control" id="lastname" name="lastname" placeholder="กรุณาใส่นามสกุล" required>
            <div class="invalid-feedback">
                กรุณาใส่นามสกุล
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-1"  >ตำแหน่ง:</div>
        <div class="col-sm-2" ><select id="position" class="form-control" name="position" class="form-control" required>
                <option selected id="p0" value="">กรุณาเลือก</option>
                <option id="p1" value="Developer">Developer</option>
                <option id="p2" value="SA">SA</option>
                <option>PM</option></select>
                <div class="invalid-feedback">
                    กรุณาเลือกหน่วยงาน
                </div>
        </div>    
        <div class="col-sm-9"></div> 
    </div>
    <div class="row">
        <div class="col-sm-2"></div>  
        <div class="col-sm-2"></div> 
        <div class="col-sm-2">
            <button type="button" class="btn btn-light btn-sm" id="clear" name="clear">Clear</button>
        </div>    
        <div class="col-sm-2" >
            <button class="btn btn-success  btn-sm" type="submit" >Save</button>
        </div>
        <div class="col-sm-4"></div>  
    </div> 
</div>

</form>
 
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
  </script>
  
<script>



$(function(){

    $('#clear').click(function (e) { 
        e.preventDefault();
            $('input').val("");
            $('#position').val("");
            $("#firstname").focus();
    });

   
    
});

(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>   
</html>