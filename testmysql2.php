<?php
$serverName ="host=localhost";
$database ="seals";
$dsn ="mysql:$serverName;dbname=$database";
$uid ="root";
$pwd ="";
try
{ 
    $pdo_object = new PDO( $dsn, $uid, $pwd);
    $pdo_object->exec("set names utf8");
    $sql = "SELECT * from person";
    $result = $pdo_object->query($sql); 
   	while ($row=$result->fetch()) {
        echo $row["prename"].$row["fname"]." ".$row["lname"]."<br>";
    }
}
catch (PDOException $e)
{
	echo 'Connection failed: ' . $e->getMessage();
}
?>