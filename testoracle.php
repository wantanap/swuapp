<?php
$tns = " 
(DESCRIPTION =
      (ADDRESS_LIST =
            (ADDRESS = (PROTOCOL = TCP)(HOST = server_ip)(PORT = server_port))
      )
      (CONNECT_DATA =
            (SERVICE_NAME = dbname)
      )
)
";
$dsn = "oci:dbname=$tns";
$uid = "user";
$pwd = "password";
try
{ 
    $pdo_object = new PDO( $dsn, $uid, $pwd);
    $sql = "SELECT sysdate from dual"; 
    $result = $pdo_object->query( $sql ); 
    while ($row=$result->fetch()) {
        echo $row[0];
    }
}
catch (PDOException $e)
{
    echo 'Connection failed: ' . $e->getMessage();
}
?>
